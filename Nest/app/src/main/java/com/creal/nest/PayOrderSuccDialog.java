package com.creal.nest;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.TextView;

import com.creal.nest.model.Order;
import com.creal.nest.util.Utils;

public class PayOrderSuccDialog extends Activity {
    public static final String INTENT_EXTRA_ORDER = "ORDER";

    public static final int ACTIVITY_ID = R.layout.dialog_order_pay_succ;
    private TextView mMsgView;
    private Order mOrder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_order_pay_succ);
        mMsgView = (TextView)findViewById(R.id.id_txt_desc);
        mOrder = getIntent().getParcelableExtra(INTENT_EXTRA_ORDER);
        if(mOrder != null) {
            String money = Utils.formatMoney(mOrder.getAmount());
            String desc = String.format(getString(R.string.pay_order_succ_msg), money, mOrder.getSalerName());
            Spannable span = new SpannableString(desc);
            span.setSpan(new ForegroundColorSpan(Color.RED), 6, 6 + money.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            mMsgView.setText(span);
        }
    }

    public void onBackPressed() {
        onBackToHomeClick(null);
    }

    public void onBackToHomeClick(View view){
        setResult(1, null);
        finish();
    }
}
