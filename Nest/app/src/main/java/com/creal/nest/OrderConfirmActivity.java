package com.creal.nest;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.creal.nest.actions.AbstractAction;
import com.creal.nest.actions.MD5PwdAction;
import com.creal.nest.model.Order;
import com.creal.nest.util.PreferenceUtil;
import com.creal.nest.util.UIUtil;
import com.creal.nest.util.Utils;
import com.creal.nest.views.HeaderView;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class OrderConfirmActivity extends Activity {
    public static final String INTENT_EXTRA_ORDER = "ORDER";

    private TextView mOrderId;
    private TextView mAmount;
    private TextView mSaler;
    private EditText mPassword;
    private Order mOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_confirm);

        HeaderView headerView = (HeaderView) findViewById(R.id.header);
        headerView.hideRightImage();
        headerView.setTitle(R.string.confirm_order_title);
        headerView.setTitleLeft();
        headerView.hideLeftBtn();

        mOrderId = (TextView)findViewById(R.id.id_txt_order_id);
        mAmount = (TextView)findViewById(R.id.id_txt_order_amount);
        mSaler = (TextView)findViewById(R.id.id_txt_order_saler);
        mPassword = (EditText)findViewById(R.id.id_txt_login_pwd);
        mOrder = getIntent().getParcelableExtra(INTENT_EXTRA_ORDER);

        mOrderId.setText(mOrder.getPrePayId());
        mAmount.setText("￥" + Utils.formatMoney(mOrder.getAmount()));
        mSaler.setText(mOrder.getSalerName());
        if(mOrder.isSkipPwd()){
            mPassword.setVisibility(View.GONE);
        }
    }

    public void onCancelClick(View view){
        setResult(0, null);
        finish();
    }

    public void onOkClick(View view){
        if(!mOrder.isSkipPwd()) {
            CharSequence password = mPassword.getText();
            if (TextUtils.isEmpty(password)) {
                Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake);
                mPassword.startAnimation(shake);
                return;
            }
        }

        String cardId = PreferenceUtil.getString(this, Constants.APP_USER_CARD_ID, null);
        Map parameters = new HashMap();
        parameters.put(Constants.KEY_CARD_ID, cardId);
        parameters.put("prepaid_id", mOrder.getPrePayId());
        parameters.put("password", mPassword.getText().toString());
        final MD5PwdAction cancelOrderAction = new MD5PwdAction(this, Constants.URL_PAY_SALER, parameters, "password");
        final Dialog progressDialog = UIUtil.showLoadingDialog(this, getString(R.string.loading), false);
        cancelOrderAction.execute(new AbstractAction.UICallBack<JSONObject>() {
            public void onSuccess(JSONObject result) {
                progressDialog.dismiss();
                Intent intent = new Intent(OrderConfirmActivity.this, PayOrderSuccDialog.class);
                intent.putExtra(PayOrderSuccDialog.INTENT_EXTRA_ORDER, mOrder);
                startActivityForResult(intent, PayOrderSuccDialog.ACTIVITY_ID);
            }

            public void onFailure(AbstractAction.ActionError error) {
                progressDialog.dismiss();
                Toast.makeText(OrderConfirmActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                //Test code below
//                Intent intent = new Intent(CancelOrderConfirmActivity.this, CancelOrderSuccDialog.class);
//                intent.putExtra(CancelOrderSuccDialog.INTENT_EXTRA_ORDER_ID, mOrderId.getText().toString());
//                startActivityForResult(intent, CancelOrderSuccDialog.ACTIVITY_ID);
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == PayOrderSuccDialog.ACTIVITY_ID){
            finish();
        }
    }
}
