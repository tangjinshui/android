package com.creal.nest.model;

import android.os.Parcel;

import com.creal.nest.actions.ActionRespObject;

import org.json.JSONException;
import org.json.JSONObject;

public class Order extends BaseModel implements ActionRespObject<Order>{

    public static final Creator<Order> CREATOR
            = new Creator<Order>() {
        public Order createFromParcel(Parcel in) {
            return new Order(in);
        }

        public Order[] newArray(int size) {
            return new Order[size];
        }
    };

    private String prePayId;
    private String salerName;
    private String originalPrice;
    private String amount;
    private boolean skipPwd;
    private String orderTime;
    private int skipPwdAmount;

    public Order(){
    }

    public Order(Parcel in){
        super(in);
        prePayId = in.readString();
        salerName = in.readString();
        originalPrice = in.readString();
        amount = in.readString();
        skipPwd = in.readInt() == 1;
        orderTime = in.readString();
        skipPwdAmount = in.readInt();
    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(prePayId);
        dest.writeString(salerName);
        dest.writeString(originalPrice);
        dest.writeString(amount);
        dest.writeInt(skipPwd ? 1 : 2);
        dest.writeString(orderTime);
        dest.writeInt(skipPwdAmount);
    }

    public String getPrePayId() {
        return prePayId;
    }

    public void setPrePayId(String prePayId) {
        this.prePayId = prePayId;
    }

    public String getSalerName() {
        return salerName;
    }

    public void setSalerName(String salerName) {
        this.salerName = salerName;
    }

    public String getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(String originalPrice) {
        this.originalPrice = originalPrice;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public boolean isSkipPwd() {
        return skipPwd;
    }

    public void setSkipPwd(boolean skipPwd) {
        this.skipPwd = skipPwd;
    }

    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    public int getSkipPwdAmount() {
        return skipPwdAmount;
    }

    public void setSkipPwdAmount(int skipPwdAmount) {
        this.skipPwdAmount = skipPwdAmount;
    }


    public static Order fromJSON(JSONObject json) throws JSONException {
        if(json == null)
            throw new IllegalArgumentException("JSONObject is null");
        Order order = new Order();
        if(json.has("prepaid_id")){
            order.setPrePayId(json.getString("prepaid_id"));
        }
        if(json.has("seller_name")){
            order.setSalerName(json.getString("seller_name"));
        }
        if(json.has("money")){
            order.setAmount(json.getString("money"));
        }
        if(json.has("original_money")){
            order.setOriginalPrice(json.getString("original_money"));
        }
        if(json.has("no_pwd_flag")){
            order.setSkipPwd("1".equals(json.getString("no_pwd_flag")));
        }
        if(json.has("order_time")){
            order.setOrderTime(json.getString("order_time"));
        }
        if(json.has("no_pwd_amount") && json.getString("no_pwd_amount").length() > 0){
            order.setSkipPwdAmount(json.getInt("no_pwd_amount"));
        }
        return order;
    }

    @Override
    public Order fillWithJSON(JSONObject jsonObject) throws JSONException {
        return fromJSON(jsonObject);
    }
}
